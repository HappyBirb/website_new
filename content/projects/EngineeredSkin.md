+++
date = "2021-11-02T19:41:01+05:30"
title = "Mechanics of Engineered Skin"
draft = false
image = "img/portfolio/EngineeredSkin.jpg"
showonlyimage = false
weight = 3
+++
soft tissue mechanics
<!--more-->

![This is photo][1]
---
*joint work with [Abby Herschman](https://www.linkedin.com/in/abby-herschman-53b906121), [Alberto Pappalardo](https://www.linkedin.com/in/alberto-pappalardo-75828212b), and [Hasan Erbil Abaci](https://www.dermatology.columbia.edu/profile/hasan-e-abaci-phd)*
#
As the largest organ of our human body, skin provides us with strong protection, critical regulation, and phenomenal sensation. Tissue engineering of human skin, aiming at reconstructing the structural and functional components, improves the quality of wound healing and scar formation. Conventionally engineered skin grafts are constructed in a generic configuration and flat in shape, thus unwearable by curve-shaped body parts such as fingers. To evaluate the mechanical performance of the conventional engineered skin and wearable engineered skin developed by *Alberto* and *Erbil*, we perform a series of uniaxial tension tests on these two skin grafts from different tissue-culturing timepoints; we combine the mechanical tests with video extensometry to eliminate the geometric effect and to capture the material's stress response; we then use second-harmonic generation microscopy to investigate the tissue's fiber structure and analyze its mechanical performance physiologically. With this study, we hope to shorten, if not to bridge, the gap of the mechanical properties between the engineered and real skin.

[1]: /img/portfolio/EngineeredSkin.jpg
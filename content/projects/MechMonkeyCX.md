+++
image = "img/portfolio/MechMonkey.jpg"
showonlyimage = false
date = "2016-11-05T19:44:32+05:30"
title = "Material Properties of Macaque Reproductive Organs"
draft = false
weight = 2
+++

soft tissue mechanics, quantitative ultrasound, comparative biology
<!--more-->

![This is photo][1]

---
*joint work with [Timothy Hall](https://www.medphysics.wisc.edu/blog/staff/hall-timothy/), [Helen Feltovich](https://intermountainhealthcare.org/find-a-doctor/f/feltovich-helen/), and [Kristin Myers](https://www.me.columbia.edu/faculty/kristin-myers)*
#
Throughout pregnancy, the reproductive organs such as uterus and cervix grow and remodel in response to the growing fetus and the expanding amniotic sac. But the mechanism and evolution of their mechanical properties are still a myth to us. Nonhuman primates such as Rhesus macaque share similarities with humans regarding maternal-fetal physiology and singleton pregnancies. Therefore, we collaborate with [Wisconsin National Primate Research Center](https://primate.wisc.edu) to study the mechanical properties of the macaque's uterus and cervix at four timepoints during gestation. Similar to our human study, we perform macro-scale mechanical tests (spherical indentation & uniaxial tension) and video extensometry to capture the tissue's force and deformation responses to various loading regimes; additionally, we conduct micro-scale measurements (nano-indentation & shear wave speed) to characterize the tissue's behavior at a close-to cellular level. Finally, we propose a constitutive material model and fit it to the experimental data using inverse finite element analysis. By comparing and constrasting the uncovered tissue behavior, we hope to outline the evolution of the macaque reproductive organs' mechanical properties throughout gestation, which, by comparative biology, enlightens us on the biomechanics of human pregnancy.

[1]: /img/portfolio/MechMonkey.jpg
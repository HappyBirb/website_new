+++
date = "2016-11-05T21:05:33+05:30"
title = "About me"
+++
![This is me][1]

Hi! I am a sixth-year PhD candidate in [Myers Soft Tissue Lab](https://kristinmyerscolumbia.com) at Columbia University's [Department of Mechanical Engineering](https://me.columbia.edu). In our lab, we study the material behavior of biological tissue to uncover the structural antecedents of preterm birth. Different from my labmates and focusing more on primate animals (human and macaque), I use optical tomography, mechanical testing (Instron), finite element analysis (FEBio & Abaqus), and comparative biology to explore the mechanical properties of the human/macaque reproductive organs. I also frequently use other computational analysis methods including R, Python and MATLAB. 
#
Before coming to New York, I earned a BS in [Mechanical Engineering](http://english.mse.hust.edu.cn) from Huazhong University of Sci. & Tech. (*Wuhan,China*), where I was a member of [Jianfeng Xu](http://mse.hust.edu.cn/info/1019/11939.htm)'s lab. I spent my senior year at UC Riverside and joined the [Regenerative Microengineering Lab](https://intra.engr.ucr.edu/~htsutsui/), where I worked on paper-based microfluidic devices.

[1]: /img/about.jpg
